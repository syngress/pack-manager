# Pack Manager - Monolit  

![RubyIMG](https://syngress.pl/images/ruby_logo.png)  

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)  

Package system allows you to generate any number of packaging with custom attributes for specific customer.  

# Things you may want to cover for local development:  

- Ruby 2.6.6
- Rails 6.1.0

# Preparation

###
```json
bundle exec rails webpacker:install
```

# Description  

Sales need to have a system that will allow to create an offer prepared for a particular client.  
An offer can contain many products.  
Every product has its own `type`, `quantity`, `price` and `custom attributes` that are specific to the type of product.  
Sales person should be able to specify quantity, type and attributes of the selected product.  
Price should be automatically calculated when selected attributes will be assigned to the product.  
Every product has its own calculation mechanism.  
System should validate parameters given by sales person when creating new offer.  
System should allow to accept or reject offer by the particular client.  

System should be secure:  
- Administrators can see all offers and all products.  
- Sales can create and manage offers and products.  
- Customer has access to their offers and see all available products.  
- Customer can reject or accept the offer (one-time choice).  

Role access security should be simple as possible, use JWT.  
System should be easily extendable in future for adding new products.  
System should be also prepared to easily filter and sort offers by product types and attributes in the future (important for data structures).  

- It should be possible to provide response in one of the chosen language (messages and errors).  

|Header                |
|----------------------|
|Accept-Language: pl_PL|
|Accept-Language: en_EN|

- Each exception should have a unique **request_id** for better and faster error finding in the future.  

JWT token should expire after 3 hours.  
Database should be created together with a predefined user (admin role).  

**request headers**

Actions requiring authorization require passing the following headers:

|Key             |Value                  |
|----------------|-----------------------|
|Content-Type    |application/json       |
|Authorization   |your_super_secret_token|

# Table of endpoint  

**CUSTOMERS**  

|Method          |Action  |Endpoint                |Note                    |Role Access  |
|----------------|--------|------------------------|------------------------|-------------|
|POST            |create  |`/rest/customers`       |Create New Customer     |Everyone		  |
|POST            |login   |`/auth/login_customer`  |Authorize Customer      |Everyone     |
|GET			       |show    |`/rest/customers/:id`   |Get Customer Object     |Everyone     |
|GET			       |index   |`/rest/customers`       |Get Customer Collection |Admin, Sales |

**create customer request**  

###
```json
{
	"company": "MyTestCompany",
	"username": "customer1",
	"email": "customer1@email.com",
	"first_name": "FirstName",
	"last_name": "LastName",
	"address": "SomeAddress 1A/37",
	"city": "TestCity",
	"country": "SomeCountry",
	"postal_code": "32-090",
	"customer_description": "SomeTestDescription",
	"password": "Qwerty11#$",
	"password_confirmation": "Qwerty11#$"
}
```  

**create customer response**  

###
```json
{
	"data": {
		"id": "1",
		"type": "customer",
		"attributes": {
			"id": 1,
			"company": "MyTestCompany",
			"username": "customer1",
			"email": "customer1@email.com",
			"first_name": "FirstName",
			"last_name": "LastName",
			"address": "SomeAddress 1A/37",
			"city": "TestCity",
			"country": "SomeCountry",
			"postal_code": "32-090",
			"customer_description": "SomeTestDescription",
			"password_digest": "$2a$12$O59/zLvDjrqSWTWr4VoZKOoAaO6z2Lr9DL3sTOifDpg21/0AUFTHW",
			"created_at": "2021-03-06T23:48:37.282+01:00",
			"updated_at": "2021-03-06T23:48:37.282+01:00"
		},
		"relationships": {
			"offers": {
			"data": []
			}
		}
	}
}
```  

**login customer request**  

###
```json
{
	"email": "customer1@email.com",
	"password": "Qwerty11#$"
}
```  

**login customer response**  

###
```json
{
	"token": "eyJhbFciOiJIUzI1NiJ9.eycjdXN0b21lcl9pZCu6NiwiZXhwIjoxNjE1MTU32Dg2fQ.ZJoiOE4IFva04ZjCbxqIk46ksP1MtQEn69w0HcGy5M4",
	"exp": "03-07-2021 02:51",
	"username": "customer1"
}
```  

**get customer object response**  

###
```json
{
	"serializer": {
		"data": {
			"id": "1",
			"type": "customer",
			"attributes": {
				"id": 1,
				"company": "MyTestCompany",
				"username": "customer1",
				"email": "customer1@email.com",
				"first_name": "FirstName",
				"last_name": "LastName",
				"address": "SomeAddress 1A/37",
				"city": "TestCity",
				"country": "SomeCountry",
				"postal_code": "32-090",
				"customer_description": "SomeTestDescription",
				"password_digest": "$2a$12$O59/zLvDjrqSWTWr4VoZKOoAaO6z2Lr9DL3sTOifDpg21/0AUFTHW",
				"created_at": "2021-03-06T23:48:37.282+01:00",
				"updated_at": "2021-03-06T23:48:37.282+01:00"
			},
			"relationships": {
				"offers": {
					"data": []
				}
			}
		}
	}
}
```  

**get customer collection response**  

###
```json
{
  "records": 2,
  "offers": {
    "data": [
      {
        "id": "1",
        "type": "customer",
        "attributes": {
          "id": 1,
          "company": "MyTestCompany",
          "username": "customer1",
          "email": "customer1@email.com",
          "first_name": "FirstName",
          "last_name": "LastName",
          "address": "SomeAddress 1A/37",
          "city": "TestCity",
          "country": "SomeCountry",
          "postal_code": "32-090",
          "customer_description": "SomeTestDescription",
          "password_digest": "$2a$12$O59/zLvDjrqSWTWr4VoZKOoAaO6z2Lr9DL3sTOifDpg21/0AUFTHW",
          "created_at": "2021-03-06T23:48:37.282+01:00",
          "updated_at": "2021-03-06T23:48:37.282+01:00"
        },
        "relationships": {
        "offers": {
          "data": []
        }
      }
    },
    {
      "id": "2",
      "type": "customer",
      "attributes": {
        "id": 2,
        "company": "MyNewTestCompany",
        "username": "customer2",
        "email": "customer2@email.com",
        "first_name": "FirstNewName",
        "last_name": "LastNewName",
        "address": "SomeAddress 1B/38",
        "city": "NewCity",
        "country": "NewCountry",
        "postal_code": "31-200",
        "customer_description": "NewTestDescription",
        "password_digest": "$20AUFTHW/zLvDjrqSWTWr4VoZKOoAaO6z2Lr9DL3sTOifDpg21/0ADL3sW",
        "created_at": "2021-03-08T21:46:32.282+01:00",
        "updated_at": "2021-03-08T21:46:32.282+01:00"
      },
      "relationships": {
        "offers": {
          "data": []
        }
      }
    }
    ]
  }
}
```  

**USERS**  

|Method          |Action  |Endpoint               |Note                |Role Access  |
|----------------|--------|-----------------------|--------------------|-------------|
|POST            |create  |`/rest/users`          |Create New User     |Admin				 |
|POST            |login   |`/auth/login_user`     |Authorize User      |Everyone     |
|GET			       |show    |`/rest/users/:id`      |Get User Object     |Admin, Sales |
|GET			       |index   |`/rest/users`          |Get User Collection |Admin        |

Users resource allow to create user with 3 allowed roles.  

- regular
- admin
- sales

**create user request**

###
```json
{
	"name": "SomeName",
	"username": "username",
	"email": "user@email.com",
	"password": "YourSuperSecretPassword",
	"password_confirmation": "YourSuperSecretPassword",
	"id_role": 2
}
```

**create user response**

###
```json
{
	"data": {
		"id": "1",
		"type": "user",
		"attributes": {
			"id": 1,
			"username": "username",
			"email": "user@email.com",
			"id_role": admin,
			"password_digest": "$2a$12$SPnx/YQ7Jq4CNhWFjWVOW.oWOFvZ1gZVOXoujVcJSO.syefb.efTC",
			"created_at": "2021-02-21T14:15:45.879+01:00",
			"updated_at": "2021-02-21T14:15:45.879+01:00"
		}
	}
}
```

The offer resource is accessed by logging in and handing over the token.  

**login user request**
###
```json
{
	"email": "user@email.com",
	"password": "YourSuperSecretPassword"
}
```

**login user response**

###
```json
{
	"token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2MTQwMDA5ODJ9.iEw-L7CAuA6sagBYy2eTm80nnsk8Z",
	"exp": "02-22-2021 14:36",
	"username": "username"
}
```

**get user response**
###
```json
{
	"serializer": {
		"data": {
			"id": "1",
			"type": "user",
			"attributes": {
				"id": 1,
				"username": "admin",
				"email": "admin@email.com",
				"id_role": "admin",
				"password_digest": "$2a$12$LPiOxNnLY4Lt52VpC34mGe7T.Mnv3WXSrPL8L1bMpJQOLjNqFcKmG",
				"created_at": "2021-03-07T20:49:22.271+01:00",
				"updated_at": "2021-03-07T20:49:22.271+01:00"
			}
		}
	}
}
```

**get user collection**
###
```json
{
  "records": 2,
  "offers": {
    "data": [
      {
        "id": "1",
        "type": "user",
        "attributes": {
          "id": 1,
          "username": "admin",
          "email": "admin@email.com",
          "id_role": "admin",
          "password_digest": "$2a$12$QPiIxNBLY4Pt52VpCG4mGe9T.Mov3EXSrML8L2bMpJQOVjNqFDKmG",
          "created_at": "2021-03-07T20:49:22.271+01:00",
          "updated_at": "2021-03-07T20:49:22.271+01:00"
        }
      },
      {
        "id": "2",
        "type": "user",
        "attributes": {
          "id": 2,
          "username": "salesman_one",
          "email": "sales@email.com",
          "id_role": "sales",
          "password_digest": "$2f$22$LStt2sLqDJLcDcHvBnaKB.ExcPPHkB4depQM7r7AI7w2Gb/TeXNBa",
          "created_at": "2021-03-07T21:02:23.275+01:00",
          "updated_at": "2021-03-07T21:02:23.275+01:00"
        }
      }
    ]
  }
}
```

**OFFERS**

|Method          |Action  |Endpoint                                |Note                   |Role Access  |
|----------------|--------|----------------------------------------|-----------------------|-------------|
|POST            |create  |`/rest/customers/:id/offers`            |Create New Offer       |Sales        |
|POST            |accept  |`/rest/customers/:id/offers/:id/accept` |Accept Offer           |Customer     |
|POST            |reject  |`/rest/customers/:id/offers/:id/reject` |Reject Offer           |Customer     |
|GET             |show    |`/rest/customers/:id/offers/:id`        |Get Offer Object       |Everyone     |
|GET             |index   |`/rest/customers/:id/offers`            |Get Offers Collection  |Admin, Sales |

You have to authorize as **sales role** to generate new offer.  
Regular user or admin cannot generate offers, an attempt to generate offer will end with the following message:   

###
```json
{
  "status": 401,
  "error": "token_invalid",
  "message": "Invalid token",
  "details": {},
  "request_id": "f07a9a1b-cdff-44cf-a452-0aaa157566f1"
}
```

Offer is generated without price.  
Price is calculated at later stages.  

**create offer request**

###
```json
{
	"name": "Test Offer 1",
	"description": "Some Offer Description",
	"user_id": 1
}
```

**create offer response**

###
```json
{
  "data": {
    "id": "1",
    "type": "offer",
    "attributes": {
      "id": 1,
      "name": "Test Offer 1",
      "description": "Some Offer Description",
      "status": "unavailable",
      "price": null,
      "user_id": 1,
      "created_at": "2021-02-21T14:48:49.898+01:00",
      "updated_at": "2021-02-21T14:48:49.898+01:00"
    },
    "relationships": {
      "products": {
          "data": []
      }
    }
  }
}
```

The offer has 4 possible statuses:  

  - **unavailable** for new no calculated offers  
  - **pending** for calculated offers  
  - **accpeted** for accepted offers
  - **rejected** for rejected offers

When user tries to accept or reject offer with an unavailable status, will receive the following message:  

**accept offer request**

###
```json
POST on /rest/customers/:id/offers/:id/accept
```

**accept offer response**

###
```json
{
  "status": 400,
  "error": "validation_failed",
  "message": "Validation failed",
  "details": "Offer unavailable",
  "request_id": "f1f6acb2-fb37-4fe0-a7bd-da7bbdba598a"
}
```

Changing offer status by a user with a regular role will be possible when offer is in the **pending** status.  

**accept offer response**

###
```json
{
  "serializer": {
    "data": {
      "id": "1",
      "type": "offer",
      "attributes": {
        "id": 1,
        "name": "Test Offer 1",
        "description": "Some Offer Description",
        "status": "accepted",
        "price": 10.0,
        "user_id": 1,
        "created_at": "2021-02-21T14:48:49.898+01:00",
        "updated_at": "2021-02-21T15:01:30.476+01:00"
      },
      "relationships": {
        "products": {
            "data": []
        }
      }
    }
  }
}
```

Another change of the same offer, whose status has been already changed by customer, will end up with the following message:  

###
```json
{
  "status": 400,
  "error": "validation_failed",
  "message": "Validation failed",
  "details": "Offer status already selected",
  "request_id": "fb7755a4-dd94-40b7-98bb-6f6c911bb59e"
}
```

**PRODUCTS**

|Method          |Action  |Endpoint                         |Note                            |Role Access |
|----------------|--------|---------------------------------|--------------------------------|------------|
|POST            |create  |`/rest/offers/:id/products`      |Create New Offer Product        |Sales       |
|GET             |show    |`/rest/offers/:id/products/:id`  |Get Offer Product Object        |Everyone    |
|GET             |index   |`/rest/offers/:id/products`      |Get Offer Products Collection   |Everyone    |     

Application allow to provide product quantity for selected offer.  
Based on this information, we will generate price multiply the quantity of products * product price.  
Product does not have a price yet, it will be calculated later.  
Logged customer has access to products assigned to his offers.  
The name of the product is important, based on product name attribute limits are validated.  
If application does not find given product name in the `config.yml` configuration, it will assign the default attribute limits.  

**create product request**

###
```json
{
	"name": "mailer_box",
	"description": "Some Product Description",
	"quantity": 200
}
```

**create product response**  

###
```json
{
  "data": {
    "id": "1",
    "type": "product",
    "attributes": {
      "id": 1,
      "name": "mailer_box",
      "description": "Some Product Description",
      "quantity": 200,
      "price": null,
      "created_at": "2021-02-21T15:17:19.280+01:00",
      "updated_at": "2021-02-21T15:17:19.280+01:00"
    },
    "relationships": {
      "type_attributes": {
        "data": []
      }
    }
  }
}
```

**TYPE ATTRIBUTES**

|Method          |Action  |Endpoint                                 |Note                               |Role Access  |
|----------------|--------|-----------------------------------------|-----------------------------------|-------------|
|POST            |create  |`/rest/products/:id/type_attributes`     |Create Product Type Attributes     |Admin, Sales |
|GET             |show    |`/rest/products/:id/type_attributes/:id` |Get Product Type Attributes Object |Admin, Sales |
|GET             |index   |`/rest/products/:id/type_attributes`     |Get Type Attributes Collection     |Admin, Sales |

Attributes assigned to the product allow to calculate price of the product and update offer price.  
Each time you add product and its attributes, it will automatically recalculate offer price.  
ObjectForms will provide us with attribute validation based on the PatternsHelper.  

**create type attributes request**

###
```json
{
	"width": 10,
	"height": 50,
	"length": 130,
	"material": []
}
```

**create type attributes response**

###
```json
{
  "data": {
    "id": "3",
    "type": "type_attribute",
    "attributes": {
      "id": 3,
      "width": 10,
      "height": 50,
      "length": 130,
      "material": [],
      "product_id": 3,
      "created_at": "2021-02-21T16:12:48.825+01:00",
      "updated_at": "2021-02-21T16:12:48.825+01:00"
    }
  }
}
```

Providing too high or too small value for the selected product attribute results with the following message:

###
```json
{
  "status": 400,
  "error": "validation_failed",
  "message": "Validation failed",
  "details": "Width value exceeded",
  "request_id": "7e89b01a-31c2-451d-a6f5-0b6593db0a64"
}
```

You can paginate and sort through collections.
**TODO**: Add search functionality.  

Ultimately, the multi-product offer after the conversion of the price is as follows

###
```json
{
  "serializer": {
    "data": {
      "id": "1",
      "type": "offer",
      "attributes": {
        "id": 1,
        "name": "Test Offer 1",
        "description": "Some Offer Description",
        "status": "pending",
        "price": "20700.3",
        "user_id": 1,
        "created_at": "2021-02-21T16:10:51.544+01:00",
        "updated_at": "2021-02-21T16:26:18.339+01:00"
      },
      "relationships": {
        "products": {
          "data": [
            {
              "id": "1",
              "type": "product"
            },
            {
              "id": "2",
              "type": "product"
            },
            {
              "id": "3",
              "type": "product"
            },
            {
              "id": "4",
              "type": "product"
            },
            {
              "id": "5",
              "type": "product"
            }
          ]
        }
      }
    }
  }
}
```

**VIEW**

Some time ago Rails used Sprockets.  
JavaScript was compiled with SprocketsRails by default and resided in `app/assets/javascripts` directory.  
From Rails `5.1+`, we could provide an option to use Webpacker as the compiler.  
From Rails `6.0+`, when we create new application, `Webpacker` gem will be installed and `webpacker:install` will be run by the Rails application generator.  
From now by default, the `app/javascript` directory will host JavaScript files.  
Existing JavaScript code for `Active Storage`, `Action Cable`, `Turbolinks`, and `Rails-UJS` will be loaded by new `application.js` pack in `app/javascript` directory.  
The `Action Cable` stubs will be created in the `app/javascripr/channels` directory and will use *ES6* instead of CoffeeScript.  

*So what is Webpacker?*  
Webpacker is a gem which allows easy integration of JavaScript pre-processor and bundler with Rails.  
The `app/javascript` directory contains:  

![webPackerStructure](https://syngress.pl/images/pack_manager/webpacker_structure.png)  

Packs directory contains entry points for webpack.  
All files in the packs directory are compiled by Webpacker.  
The `application.js` pack by default includes JavaScript code for Active Storage, Action Cable, Turbolinks, and Rails-UJS.  

###
```json
require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
```

We will use latest bootstrap based layout for responsive views.  
Access to resources will be secured by JWT token, logging in takes place from the login form.  

![webPackerLoginPage](https://syngress.pl/images/pack_manager/login_page.png)  

After successful login JWT token is transferred via backend to redis (TTL was set to `3 hours`).  
During this time, JWT will be stored in Redis InMemory Database under `key_name` that will be associated with browser encrypted cookie which values indicate customer's username and JWT token.  
Each request requiring authorization verifies provided token, is it able to obtain customer object from the database.  
If customer object has been found, application additionally verify value stored in the encrypted cookie, three conditions must be met:  
- JWT token from encrypted cookie, must be valid and allow to get customer object from database.  
- The `username` value from the encrypted cookie, must match the value of the customer object `username` attribute.  
- JWT token from `Redis` must be same as JWT token stored in encrypted cookie.  

If any of the conditions are not met, customer will be redirect to the login page.  

**VIEW TABLES**

Application use `DataGrid` gem to generate collections in tables.  
New responsive bootstrap give us nice visual experience.  
Since we use `Kaminari` gem for pagination, Datagrid will provide us with quick implementation of the pagination functionality.  
Unfortunately, sorting looks different, Datagrid passes `descending` attribute in params attrs.  
We need to make small workaround to adapt to the current logic.  

![webPackerOffers](https://syngress.pl/images/pack_manager/offers_page.png)  

GUI enables rejection or acceptance of an offer.  
Condition for displaying modal is that the offer must be in the `pending` status.  

![webPackerOffersPending](https://syngress.pl/images/pack_manager/offers_pending_page.png)  

Offer status may be selected only once.  
Changing offer status prevents modal from being displayed again.  

![webPackerOffersModal](https://syngress.pl/images/pack_manager/offers_modal_page.png)  

You can view details of the offer from the offers list.  
Details show offer with its status and price.  
In addition, details include offer products.  
Instead of the DataGrid, we can generate simple nice tables with bootstrap, DataGrid should be used for larger sets of data.  

![webPackerOfferPage](https://syngress.pl/images/pack_manager/offer_page.png)  

Available offer variants are generated dynamically from application configuration `config/settings.yml`.  
Offer variants are set under the `product_type_attributes` configuration key, ex:  

###
```json
product_type_attributes:
  default:
    minimum_quantity: 100
    maximum_quantity: 500
    width: [1,100]
    height: [1,100]
    length: [1,100]
    material: [black,solid]
```

Above solution allows to add more offers to the main page.  
Configuration hash shouldn't be a problem either.  

![webPackerIndex](https://syngress.pl/images/pack_manager/index_page.png)  

Immediately after sign in, customer is redirected to profile page.  
Profile page display form that allows customer to update profile.  
Application uses gem `SimpleForm` to generate forms.  

![WebPackerProfile](https://syngress.pl/images/pack_manager/profile_page.png)  

ActiveRecord validate form fields.  
Note that REST requests are additionally validated through `Dry::Validation` but uses same regexp validation from `PatternsHelper`.  

![WebPackerProfileError](https://syngress.pl/images/pack_manager/profile_error_page.png)

Logged in user has access to all products that belong to the user's offers, we can sort by columns:  

- id
- quantity
- price
- offer_id
- created_at

![WebPackerProductsPage](https://syngress.pl/images/pack_manager/products_page.png)  

Product page displays related offers allowing to go directly to the offer details.  

![WebPackerProductsPageOffer](https://syngress.pl/images/pack_manager/products_page_offer.png)  

**RSPEC TEST**

Add some sample tests - model / controller / routing   
