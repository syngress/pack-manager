require_relative "boot"

require "rails/all"
require_relative '../lib/middleware/setup_request_id'
require_relative '../lib/middleware/set_locale'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PackManager
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1
    config.autoload_paths += %W( lib/ )
    config.i18n.default_locale = Settings.i18n.default_locale
    config.i18n.available_locales = %i[en_GB pl_PL]
    config.i18n.enforce_available_locales = false
    config.i18n.fallbacks = Settings.i18n.fallbacks
    config.api_only = false
    config.force_ssl = false
    config.log_level = :debug
    config.active_record.cache_versioning = false

    config.exceptions_app = ->(env) { MiddlewareErrorsController.action(:show).call(env) }
    config.time_zone = 'Warsaw'
    config.middleware.use ActionDispatch::Flash
    config.middleware.insert_after Rack::Head, Middleware::SetupRequestId
    config.middleware.insert_before Middleware::SetupRequestId ,Middleware::SetLocale
    config.middleware.use ActionDispatch::Cookies
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post delete put patch options head]
      end
    end
  end
end
