# frozen_string_literal: true

# INITIALIZE SIDEKIQ REDIS
REDIS_SETTINGS = if Settings.redis.sentinels?
                   {
                     url: "redis://#{Settings.redis.master_name}",
                     role: :master,
                     db: Settings.redis.db,
                     sentinels: Settings.redis.sentinels.map(&:to_h)
                   }
                 else
                   {
                     host: Settings.redis.host,
                     port: Settings.redis.port,
                     db: Settings.redis.db
                   }
                 end

REDIS = Redis.new(REDIS_SETTINGS)

# INITIALIZE CACHE
Rails.application.config.cache_store = :redis_store, REDIS_SETTINGS.merge(namespace: :cache, expires_in: Settings.cache.default.expires_in)
Rails.cache = ActiveSupport::Cache.lookup_store(Rails.application.config.cache_store)
