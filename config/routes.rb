Rails.application.routes.draw do
  root 'home#index'
  get 'home/login'
  get 'home/logout'
  get 'home/register'
  get 'application/client_authorization'

  resources :offers, only: %i[index show] do
    post '/accept', to: 'offers#accept', :on => :member
    post '/reject', to: 'offers#reject', :on => :member
  end

  resources :customers, only: %i[show create new update edit]
  resources :products, only: %i[index]

  namespace :rest do
    resources :offers, only: [] do
      resources :products, only: %i[index show create]
    end

    resources :products, only: [] do
      resources :type_attributes, only: %i[index show create]
    end

    resources :users, param: :id, only: %i[index show create]
    resources :customers, param: :id, only: %i[index show create] do
      resources :offers, only: %i[index show create]
      post 'offers/:id/accept', to: 'offers#accept'
      post 'offers/:id/reject', to: 'offers#reject'
    end
  end
    post '/auth/login_user', to: 'authentication#login_user'
    post '/auth/login_customer', to: 'authentication#login_customer'
end
