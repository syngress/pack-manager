# frozen_string_literal: true

# lib/middleware
module Middleware
  # lib/middleware/setup_request_id.rb
  class SetupRequestId
    def initialize(app)
      @app = app
    end

    def call(env)
      fcs_request_id = SecureRandom.uuid
      clean_thread
      Thread.current[:request_id] = fcs_request_id
      @app.call(env).tap { |_status, headers, _body| headers['X-Fcs-Request-Id'] = fcs_request_id }
    end

    private

    def clean_thread
      Thread.current[:exception] = nil
      Thread.current[:request_id] = nil
    end
  end
end
