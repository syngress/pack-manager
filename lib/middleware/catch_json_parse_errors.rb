# frozen_string_literal: true

module Middleware
  # /lib/middleware/catch_json_parse_errors.rb
  class CatchJsonParseErrors
    def initialize(app)
      @app = app
    end

    def call(env)
      @app.call(env)
    rescue ActionDispatch::Http::Parameters::ParseError => e
      raise e unless env['HTTP_ACCEPT'] =~ %r{ application/json } || env['CONTENT_TYPE'] =~ %r{ application/json }

      [
        400, { 'Content-Type' => 'application/json' },
        [{
          "status": 400,
          "error": :json_parse_error,
          "message": I18n.t(:json_parse_error, scope: 'errors.messages'),
          "details": e.to_s
        }.to_json]
      ]
    end
  end
end
