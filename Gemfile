source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.0'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis-rails', '5.0.2'
# Postgres Database
gem 'pg', '~> 1.2'
gem 'bcrypt', '~> 3.1'
gem 'jwt', '~> 2.2'
gem 'jsonapi-serializer', '~> 2.1'
gem 'kaminari', '1.2.1'
gem 'config', '2.2.1'
gem 'sidekiq', '6.1.0'
gem 'sidekiq-monitor-stats', '0.0.2'
gem 'rack-protection', '2.0.8.1'
gem 'rack-cors', '1.1.1'
gem 'rubocop', '0.87.1'
gem 'dry-schema', '~> 1.5'
gem 'dry-struct', '~> 1.3'
gem 'dry-types', '~> 1.4'
gem 'dry-validation', '~> 1.5'
gem 'seed-fu', '~> 2.3'
gem 'webpacker', '~> 5.2'
gem 'jquery-rails', '~> 4.4'
gem 'datagrid', '~> 1.6'
gem 'simple_form', '~> 5.1'
gem 'country_select', '~> 5.0'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'pry', '0.11.1'
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  gem 'spring'
end

group :development, :test do
  gem 'webdrivers'
  gem 'rspec-rails', '~> 4.0'
  gem 'rspec-its', '~> 1.3'
  gem 'webmock', '~> 3.11'
  gem 'factory_bot_rails', '~> 6.1'
  gem 'shoulda-matchers', '~> 4.5'
  gem 'database_cleaner', '~> 2.0'
  gem 'faker', '~> 2.16'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
