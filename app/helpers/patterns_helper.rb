# frozen_string_literal: true

# app/helpers/patterns_helper.rb
module PatternsHelper
  NAME_PATTERN = /\A[a-zA-ZzżźćńółęąśŻŹĆĄŚĘŁÓŃ'-]*\z/i
  POSTAL_CODE_PATTERN = /\A([0-9]{2})(-[0-9]{3})?\z/i
  EMAIL_PATTERN = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  COUNTRY_PATTERN = /\A([a-z]{2})\z/i
  CITY_PATTERN = /\A[a-zA-ZzżźćńółęąśŻŹĆĄŚĘŁÓŃ '-]*\z/i

  def self.name_pattern
    NAME_PATTERN
  end

  def self.postal_code_pattern
    POSTAL_CODE_PATTERN
  end

  def self.email_pattern
    EMAIL_PATTERN
  end

  def self.country_pattern
    COUNTRY_PATTERN
  end

  def self.city_pattern
    CITY_PATTERN
  end
end
