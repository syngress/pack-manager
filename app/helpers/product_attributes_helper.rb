# frozen_string_literal: true

# app/helpers/product_attributes_helper.rb
module ProductAttributesHelper
  def self.attributes(product_name)
    process_attributes(product_name)
  end

  def self.process_attributes(product_name)
    product_name.to_sym.in?(product_names) ? type_attributes(product_name) : default_attributes
  end

  def self.product_names
    Settings.product_type_attributes.keys
  end

  def self.type_attributes(product_name)
    Settings.product_type_attributes["#{product_name}"]
  end

  def self.default_attributes
    Settings.product_type_attributes.default
  end
end
