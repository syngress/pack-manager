# frozen_string_literal: true

# app/helpers/price_calculation.rb
module PriceCalculation
  def self.process_price(product_attributes)
    width    = product_attributes[:width] || 0
    height   = product_attributes[:height] || 0
    length   = product_attributes[:length] || 0
    material = product_attributes[:material] || []
    product  = Product.find(product_attributes[:product_id])

    price = (width + height + length) * 0.1 * product.quantity + additional_material_price(material)

    update_price_product(price, product)
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def self.additional_material_price(material)
    material.include?('transparent') ? 0.15 : 0.00
  end

  def self.update_price_product(price, product)

    product.update(price: price)
    update_offer(price, product)
  end

  def self.update_offer(product_price, product)
    offer_price = product.offer.price || 0.00
    new_offer_price = offer_price + product_price

    product.offer.update(price: new_offer_price, status: 'pending')
  end
end
