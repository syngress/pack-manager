# frozen_string_literal: true

# app/helpers/role_names.rb
module RoleNames
  def self.translate_role(user)
    case true
    when user.is_admin?
      'admin'
    when user.is_salesman?
      'sales'
    when user.is_regular?
      'regular'
    else
      user.id_role
    end
  end
end
