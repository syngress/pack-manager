module ApplicationHelper
  def display_flash(key, value)
    div_class = [bootstrap_class_for(key), 'text-center fade show alert-dismissible'].join(' ')
    div_style_override =['height: 100%;']

    content_tag(:div, class: div_class, style: div_style_override) do
      content_tag(:div, value)
    end
  end

  def bootstrap_class_for(flash_type)
    case flash_type.to_sym
    when :notice then "alert-info"
    when :success then "alert-success"
    when :error then "alert-danger"
    when :alert then "alert-danger"
    else "alert-#{flash_type}"
    end
  end
end
