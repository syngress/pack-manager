# frozen_string_literal: true

module OfferService
  # app/services/offer_services/get_offer_service.rb
  class GetOfferService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @authorized_user = attrs.fetch(:authorized_user)
      @params = attrs.fetch(:params)
    end

    def call
      offers
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :authorized_user, :params

    def offers
      authorized_user.is_regular? ? customer_offers : all_offers
    end

    def customer_offers
      authorized_user.offers.order(order).page(page).per(per_page)
    end

    def all_offers
      Customer.find(params[:customer_id]).offers.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
