# frozen_string_literal: true

module OfferService
  # app/services/offer_service/create_offer_service.rb
  class CreateOfferService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @authorized_user = attrs.fetch(:authorized_user)
      @params = attrs.fetch(:params)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      check_role
      build_result(create_offer)
    end

    private

    attr_reader :form, :authorized_user, :params

    def create_offer
      @create_offer ||= Customer.find(params['customer_id']).offers.create(form.to_hash.merge!(status: 'unavailable'))
    end

    def check_role
      raise Error::Unauthorized.new if !authorized_user.is_salesman?
    end
  end
end
