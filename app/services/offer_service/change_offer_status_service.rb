# frozen_string_literal: true

module OfferService
  # app/services/offer_service/change_offer_status_service.rb
  class ChangeOfferStatusService
    include Service

    def initialize(attrs = {})
      @offer_status = attrs.fetch(:offer_status)
      @offer = attrs.fetch(:offer)
      @params = attrs.fetch(:params)
    end

    def call
      check_offer_status
      authorize_status_change
      update_offer
    end

    private

    attr_reader :offer_status, :offer, :authorized_user, :params

    def check_offer_status
      raise Error::ValidationFailed.new(I18n.t(:offer_unavailable, scope: 'errors.messages')) if offer.status == 'unavailable'
      raise Error::ValidationFailed.new(I18n.t(:offer_already_selected, scope: 'errors.messages')) if offer.status.in?(['accepted', 'rejected'])
    end

    def authorize_status_change
      if params[:customer_id].to_i != offer.customer_id
        raise Error::Unauthorized.new
      end
    end

    def update_offer
      offer.update_attribute(:status, offer_status)
    end
  end
end
