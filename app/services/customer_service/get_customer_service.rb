# frozen_string_literal: true

module CustomerService
  # app/services/customer_services/get_customer_service.rb
  class GetCustomerService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @authorized_user = attrs.fetch(:authorized_user)
    end

    def call
      check_role
      customers_collection
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :authorized_user

    def customers_collection
      Customer.all.order(order).page(page).per(per_page)
    end

    def check_role
      raise Error::Unauthorized.new if authorized_user.is_regular?
    end

    def order
      { sort_by => sort_order }
    end
  end
end
