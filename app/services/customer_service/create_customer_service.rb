# frozen_string_literal: true

module CustomerService
  # app/services/customer_service/create_customer_service.rb
  class CreateCustomerService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      build_result(create_customer)
    end

    private

    attr_reader :form

    def create_customer
      Customer.create(form.to_hash)
    end
  end
end
