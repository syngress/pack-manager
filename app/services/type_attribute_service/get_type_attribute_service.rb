# frozen_string_literal: true

module TypeAttributeService
  # app/services/type_attribute_services/get_type_attribute_service.rb
  class GetTypeAttributeService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @product = attrs.fetch(:product)
    end

    def call
      type_attributes
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :product

    def type_attributes
      product.type_attributes.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
