# frozen_string_literal: true

module TypeAttributeService
  # app/services/type_attribute_service/create_type_attribute_service.rb
  class CreateTypeAttributeService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @authorized_user = attrs.fetch(:authorized_user)
      @params = attrs.fetch(:params)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      check_role
      calculate_price_for_product
      build_result(create_type_attribute)
    end

    private

    attr_reader :form, :authorized_user, :params

    def create_type_attribute
      Product.find(params[:product_id]).type_attributes.create(form.to_hash)
    end

    def calculate_price_for_product
      PriceCalculation.process_price(form.to_hash)
    end

    def check_role
      raise Error::Unauthorized.new if authorized_user.is_regular?
    end
  end
end
