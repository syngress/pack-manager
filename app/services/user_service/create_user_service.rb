# frozen_string_literal: true

module UserService
  # app/services/user_service/create_user_service.rb
  class CreateUserService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      build_result(create_user)
    end

    private

    attr_reader :form

    def create_user
      User.create(form.to_hash)
    end
  end
end
