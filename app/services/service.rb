# frozen_string_literal: true

# /app/services/service.rb
module Service
  extend ActiveSupport::Concern
  # class method
  module ClassMethods
    def call(attrs = {})
      new(attrs).call
    end
  end

  private

  attr_reader :form

  def validation_error
    Error::ValidationFailed.new(form_errors)
  end

  def build_result(object = nil)
    ServiceResult.new(object)
  end

  def form_errors
    form.errors if form.errors?
    form.schema.errors.first.text if form.schema.errors.present?
  end
end
