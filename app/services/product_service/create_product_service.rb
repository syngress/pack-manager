# frozen_string_literal: true

module ProductService
  # app/services/product_service/create_product_service.rb
  class CreateProductService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @authorized_user = attrs.fetch(:authorized_user)
      @params = attrs.fetch(:params)
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      check_role
      build_result(create_product)
    end

    private

    attr_reader :form, :authorized_user, :params

    def create_product
      Offer.find(params[:offer_id]).products.create(form.to_hash.merge!(offer_id: params[:offer_id]))
    end

    def check_role
      raise Error::Unauthorized.new if !authorized_user.is_salesman?
    end
  end
end
