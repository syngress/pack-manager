# frozen_string_literal: true

module ProductService
  # app/services/product_services/get_product_service.rb
  class GetProductService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @authorized_user = attrs.fetch(:authorized_user)
      @params = attrs.fetch(:params)
    end

    def call
      products
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :authorized_user, :params

    def products
      authorized_user.is_regular? ? customer_products : all_products
    end

    def customer_products
      authorized_user.offers.find(params[:offer_id]).products.order(order).page(page).per(per_page)
    end

    def all_products
      Offer.find(params[:offer_id]).products.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
