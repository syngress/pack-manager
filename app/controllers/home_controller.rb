# frozen_string_literal: true

# app/controllers/home_controller.rb
class HomeController < ApplicationController
  before_action :logged_in

  def index
    @customer = customer if logged_in
    @product_attrs = Settings.product_type_attributes
  end

  def login; end

  def logout
    if logged_in
      logout_client
      redirect_to home_login_path, flash: { success: I18n.t(:successfully_logout, scope: 'locale.messages') }
    else
      flash.now[:notice] = I18n.t(:not_logged_in, scope: 'locale.messages')
      render action: :login
    end
  end

  def register; end

  private

  def customer
    @customer ||= authorize_client_request
  end

  def logged_in
    @logged_in = logged_in?
  end
end
