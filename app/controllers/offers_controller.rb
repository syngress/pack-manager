# frozen_string_literal: true

# app/controllers/offers_controller.rb
class OffersController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_client_request, only: %i[show index]
  before_action :logged_in

  def show
    @offer = offer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(datagrid_sort: params[:customer_offers_grid])
    @offers_grid = CustomerOffersGrid.new(params[:offers_grid]) do |scope|
      scope.find(@customer.id).offers.order(order).page(page).per(per_page)
    end
  end

  def accept
    change_offer_status('accepted')
    flash[:success] = I18n.t(:accepted, scope: 'locale.messages.offer')
    render js: "window.location = '#{offers_path}'"
  end

  def reject
    change_offer_status('rejected')
    flash[:success] = I18n.t(:rejected, scope: 'locale.messages.offer')
    render js: "window.location = '#{offers_path}'"
  end

  private

  def offer
    @offer ||= Offer.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def change_offer_status(status)
    OfferService::ChangeOfferStatusService.call(
      offer_status: status,
      offer: offer,
      params: params.merge!(customer_id: offer.customer.id)
    )
  rescue StandardError => error
    flash.now[:error] = error
    render :index
  end

  def order
    { sort_by => sort_order }
  end

  def params_for_sort
    ::Offers::ParamsForSort.allowed_values
  end

  def logged_in
    @logged_in = logged_in?
  end
end
