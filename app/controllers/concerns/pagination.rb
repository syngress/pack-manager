# frozen_string_literal: true

# /app/controllers/concerns/pagination
module Pagination
  extend ActiveSupport::Concern

  attr_reader :page, :per_page

  def setup_pagination(options = {})
    @page = options[:page].try(:to_i)
    @per_page = options[:per_page].try(:to_i)
    @page = 1 unless page.in?(1..1_000_000)
    @per_page = default unless per_page.in?(min..max)
  end

  def offset
    per_page * (page - 1)
  end

  private

  def default
    15
  end

  def min
    1
  end

  def max
    1000
  end
end
