# frozen_string_literal: true

# app/controllers/concerns/exception_handler.rb
module ExceptionHandler
  extend ActiveSupport::Concern

  class AuthenticationError < StandardError; end

  included do
    # Define custom handlers
    rescue_from ActiveRecord::RecordInvalid, with: :invalid_request
    # rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    # rescue_from ExceptionHandler::MissingToken, with: :unauthorized_request
    # rescue_from ExceptionHandler::InvalidToken, with: :unauthorized_request

    rescue_from ActiveRecord::RecordNotFound do |error|
      json_response({ message: error.message }, :not_found)
    end
  end

  private

  # JSON response with message; Status code 422 - unprocessable entity
  def invalid_request(error)
    json_response({ message: error.message }, :unprocessable_entity)
  end

  # JSON response with message; Status code 401 - Unauthorized
  def unauthorized_request(error)
    json_response({ message: error.message }, :unauthorized)
  end
end
