# frozen_string_literal: true

# app/controllers/concerns/error_handler.rb
module ErrorHandler
  extend ActiveSupport::Concern

  included do
    rescue_from StandardError, with: :handle
  end

  def routing_not_found
    render_error Error::ResourceNotFound.new("No route matches #{request.method} #{params[:path]}")
  end

  private

  def handle(error)
    log_error(error)
    case error
    when Error
      render_error(error)
    when ActionController::ParameterMissing
      message = "#{error.message}.".capitalize
      render_error Error::InvalidParameter.new(nil, message: message)
    when ActiveRecord::RecordNotFound
      render_error Error::ResourceNotFound.new(error.message)
    else
      render_error Error::InternalServerError.new(error)
    end
  end

  def log_error(error)
    logger.error "#{error.class}: #{error.message}"
    logger.error error.backtrace.join("\n")
  end

  def render_error(object)
    render json: object, serializer: ErrorSerializer, status: object.http_status, root: 'error'
  end
end
