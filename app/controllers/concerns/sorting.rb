# frozen_string_literal: true

# /app/controllers/concerns/sorting
module Sorting
  extend ActiveSupport::Concern

  def setup_sorting(options = {})
    @sort_by = options[:sort_by].try(:to_sym)
    @sort_order = options[:sort_order].try(:to_sym)
    @params_for_sort = options[:params_for_sort]
    @datagrid_sort = options[:datagrid_sort]
    datagrid_sort_set
    sorting_params
  end

  private

  def sorting_params
    raise Error::InvalidParameter.new :sort_by, value: @sort_by unless @sort_by.in?(sort_by_parameter) || @sort_by.blank?
    raise Error::InvalidParameter.new :sort_order, value: @sort_order unless @sort_order.in?(%i[asc desc]) || @sort_order.blank?
  end

  def order
    { sort_by => sort_order }
  end

  def sort_by
    @sort_by || default_sort_by
  end

  def default_sort_by
    :id
  end

  def sort_order
    @sort_order || default_sort_order
  end

  def default_sort_order
    :desc
  end

  def sort_by_parameter
    @params_for_sort || %i[id]
  end

  def datagrid_sort_set
    if @datagrid_sort.present?
      @sort_order = ActiveRecord::Type::Boolean.new.cast(@datagrid_sort[:descending]) ? :desc : :asc
    end
  end
end
