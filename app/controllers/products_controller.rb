# frozen_string_literal: true

# app/controllers/products_controller.rb
class ProductsController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_client_request, only: %i[index]
  before_action :logged_in

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(datagrid_sort: params[:products_grid])
    @products_grid = ProductsGrid.new(params[:page]) do |scope|
      scope.where(customer_id: authorize_client_request.id)
           .joins(:products)
           .select("products.*")
           .group("offers.id, products.id")
           .order(order)
    end
  end

  private

  def order
    "#{sort_by} #{sort_order}"
  end

  def logged_in
    @logged_in = logged_in?
  end

  def sort_column
    if params[:products_grid].present?
      params[:products_grid][:order]
    else
      sort_by.to_s
    end
  end
end
