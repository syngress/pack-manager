# frozen_string_literal: true

# app/controllers/customer_controller.rb
class CustomersController < ApplicationController
  before_action :authorize_client_request
  before_action :logged_in
  before_action :set_customer, only: %i[show edit update]

  def create; end

  def new; end

  def show; end

  def edit; end

  def update
    if @customer.update!(customer_params)
      redirect_to edit_customer_path, flash: { success: "Customer successfully updated" }
    else
      render :edit
    end
  rescue StandardError => error
    flash.now[:error] =  @customer.errors.full_messages.join("<br />").html_safe
    render :edit
  end

  private

  def set_customer
    @customer = Customer.find(params[:id])
  end

  def customer_params
    params.require(:customer).permit(
      :company,
      :username,
      :email,
      :first_name,
      :last_name,
      :address,
      :city,
      :country,
      :postal_code,
      :customer_description
    )
  end

  def logged_in
    @logged_in = logged_in?
  end
end
