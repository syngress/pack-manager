# frozen_string_literal: true

# app/controllers/authentication_controller.rb
class AuthenticationController < ApplicationController
  before_action :authorize_request, except: %i[login_user login_customer]

  # POST /auth/login_user
  def login_user
    @user = User.find_by_email(params[:email])
    if @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      time = Time.now + 3.hours.to_i
      render json: { token: token, exp: time.strftime('%m-%d-%Y %H:%M'),
                     username: @user.username }, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  # POST /auth/login_customer
  def login_customer
    @customer = Customer.find_by_email(params[:email])
    if @customer&.authenticate(params[:password])
      token = JsonWebToken.encode(customer_id: @customer.id)
      time = Time.now + 3.hours.to_i
      cookies.encrypted[:packmanager] = {
        value: {
          username: @customer.username,
          token: token
        },
        expires: 3.hours
      }
      render json: { token: token, exp: time.strftime('%m-%d-%Y %H:%M'),
                     username: @customer.username, id: @customer.id }, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  private

  def login_params
    params.permit(:email, :password)
  end
end
