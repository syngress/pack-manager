# frozen_string_literal: true

# app/controllers/application_controller.rb
class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  include Response
  include ExceptionHandler
  include ErrorHandler

  def not_found
    raise Error::ResourceNotFound
  end

  # Authorize request from REST API
  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header

    begin
      decoded = JsonWebToken.decode(header)
      decoded.key?('customer_id') ? Customer.find(decoded[:customer_id]) : User.find(decoded[:user_id])
    rescue ActiveRecord::RecordNotFound
      raise Error::Unauthorized
    rescue JWT::DecodeError
      raise Error::Unauthorized
    end
  end

  # Login Client from browser
  def client_authorization
    return unless %i[token username].all? { |k| params.key? k }

    REDIS.setex(authorized_cookie['username'], 108_00, params[:token])
  end

  # Authorize client request is performed with (selected) controller action
  def authorize_client_request
    @customer = Customer.find(decode_jwt[:customer_id])

    if @customer.username != authorized_cookie['username'] || redis_jwt_token != authorized_cookie['token']
      redirect_to home_login_path
    end

    @customer
  rescue ActiveRecord::RecordNotFound
    redirect_to home_login_path
  rescue JWT::DecodeError
    redirect_to home_login_path
  end

  # Decode information from token
  def decode_jwt
    return {} if authorized_cookie.nil?
    authorized_cookie['token'].nil? ? {} : JsonWebToken.decode(authorized_cookie['token'])
  end

  # Get token from redis IMDB
  def redis_jwt_token
    @redis_jwt_token ||= REDIS.get(authorized_cookie['username'])
  end

  # Logout action remove token from redis and destroy cookie
  def logout_client
    REDIS.del(authorized_cookie['username'])
    cookies.delete :packmanager

    return
  end

  # Get information from encrypted cookie
  def authorized_cookie
    return {} unless cookies[:packmanager].present?

    cookie = URI.unescape(cookies[:packmanager])
    data, iv, auth_tag = cookie.split('--').map do |v|
      Base64.strict_decode64(v)
    end
    cipher = OpenSSL::Cipher.new('aes-256-gcm')
    secret_key_base = Rails.application.secret_key_base
    secret = OpenSSL::PKCS5.pbkdf2_hmac_sha1(secret_key_base, 'authenticated encrypted cookie', 1000, cipher.key_len)

    cipher.decrypt
    cipher.key = secret
    cipher.iv  = iv
    cipher.auth_tag = auth_tag
    cipher.auth_data = ''

    cookie_payload = cipher.update(data)
    cookie_payload << cipher.final
    cookie_payload = JSON.parse cookie_payload

    # Decode Base64 encoded authorized username
    decoded_stored_value = Base64.decode64 cookie_payload['_rails']['message']
    JSON.parse decoded_stored_value
  end


  # Check that the customer is logged in
  def logged_in?
    redis_jwt_token.present? && cookies[:packmanager].present? && redis_jwt_token == authorized_cookie['token']
  end
end
