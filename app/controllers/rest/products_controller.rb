# frozen_string_literal: true

# app/controllers/rest/products_controller.rb
class Rest::ProductsController < ApplicationController
  before_action :product, only: %i[show]
  include Pagination
  include Sorting

  def show
    raise Error::Unauthorized.new if offer.customer.id != authorize_request.id && authorize_request.is_regular?

    render json: {
      serializer: ProductSerializers::ProductSerializer.new(
        product
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      records: products.count,
      offers: ProductSerializers::ProductSerializer.new(
        products
      ).serializable_hash
    }
  end

  def create
    form = ProductForms::CreateProductForm.new(product_params)
    result = ProductService::CreateProductService.call(form: form, authorized_user: authorize_request, params: params)

    if result.error?
      render_error(result.object)
    else
      render json: ProductSerializers::ProductSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def product
    @product ||= offer.products.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def offer
    @offer ||= Offer.find(params[:offer_id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def products
    @products ||= ProductService::GetProductService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      authorized_user: authorize_request,
      params: params
    )
  end

  def product_params
    params.permit(:name, :description, :quantity, :price).to_h.symbolize_keys
  end

  def params_for_sort
    ::Products::ParamsForSort.allowed_values
  end
end
