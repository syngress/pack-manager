# app/controllers/rest/users_controller.rb
class Rest::UsersController < ApplicationController
  include Pagination
  include Sorting

  before_action :user, except: %i[index create]

  def show
    raise Error::Unauthorized.new if authorize_request.is_regular?

    render json: {
      serializer: UserSerializers::UserSerializer.new(
        user
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      records: users_collection.count,
      offers: UserSerializers::UserSerializer.new(
        users_collection
      ).serializable_hash
    }
  end

  def create
    raise Error::Unauthorized.new if !authorize_request.is_admin?

    form = UserForms::UserForm.new(user_params)
    result = UserService::CreateUserService.call(form: form)

    if result.error?
      render_error(result.object)
    else
      render json: UserSerializers::UserSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def user
    @user ||= User.find(params[:id])
  end

  def users_collection
    @users_collection ||= UserService::GetUserService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      authorized_user: authorize_request
    )
  end

  def user_params
    params.permit(
      :id,
      :username,
      :email,
      :password,
      :password_confirmation,
      :id_role
    ).to_h.symbolize_keys
  end

  def params_for_sort
    ::Users::ParamsForSort.allowed_values
  end
end
