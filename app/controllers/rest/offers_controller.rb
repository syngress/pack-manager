# frozen_string_literal: true

# app/controllers/rest/offers_controller.rb
class Rest::OffersController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_request
  before_action :offer, only: %i[show accept reject]

  def show
    if params[:customer_id].to_i != authorize_request.id && authorize_request.is_regular?
      raise Error::Unauthorized.new
    end

    render json: {
      serializer: OfferSerializers::OfferSerializer.new(
        offer
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      records: offers_collection.count,
      offers: OfferSerializers::OfferSerializer.new(
        offers_collection
      ).serializable_hash
    }
  end

  def create
    form = OfferForms::CreateOfferForm.new(offer_params)
    result = OfferService::CreateOfferService.call(form: form, authorized_user: authorize_request, params: params)

    if result.error?
      render_error(result.object)
    else
      render json: OfferSerializers::OfferSerializer.new(result.object).serializable_hash.to_json
    end
  end

  def accept
    OfferService::ChangeOfferStatusService.call(
      offer_status: 'accepted',
      offer: offer,
      authorized_user: authorize_request,
      params: params
    )

    show
  end

  def reject
    OfferService::ChangeOfferStatusService.call(
      offer_status: 'rejected',
      offer: offer,
      authorized_user: authorize_request,
      params: params
    )

    show
  end

  private

  def offer
    @offer ||= Customer.find(params[:customer_id]).offers.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def offers_collection
    @offers_collection ||= OfferService::GetOfferService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      authorized_user: authorize_request,
      params: params
    )
  end

  def offer_params
    params.permit(:name, :description, :user_id).to_h.symbolize_keys
  end

  def params_for_sort
    ::Offers::ParamsForSort.allowed_values
  end
end
