# frozen_string_literal: true

# app/controllers/rest/type_attributes_controller.rb
class Rest::TypeAttributesController < ApplicationController
  before_action :type_attributes, only: %i[show]
  include Pagination
  include Sorting

  def show
    check_role
    render json: {
      serializer: TypeAttributeSerializers::TypeAttributeSerializer.new(
        type_attributes
      ).serializable_hash
    }
  end

  def index
    check_role
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      records: type_attributes_collection.count,
      offers: TypeAttributeSerializers::TypeAttributeSerializer.new(
        type_attributes_collection
      ).serializable_hash
    }
  end

  def create
    form = TypeAttributeForms::TypeAttributeForm.new(type_attribute_params.merge!(product_id: params[:product_id]))
    result = TypeAttributeService::CreateTypeAttributeService.call(form: form, authorized_user: authorize_request, params: params)

    if result.error?
      render_error(result.object)
    else
      render json: TypeAttributeSerializers::TypeAttributeSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def type_attributes
    @type_attributes ||= product.type_attributes.find(params['id'])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def product
    @product ||= Product.find(params['product_id'])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound, e
  end

  def type_attributes_collection
    @type_attributes_collection ||= TypeAttributeService::GetTypeAttributeService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      product: product
    )
  end

  def type_attribute_params
    params.permit(
      :width,
      :height,
      :length,
      :material => []
    ).to_h.symbolize_keys
  end

  def params_for_sort
    ::TypeAttributes::ParamsForSort.allowed_values
  end

  def check_role
    raise Error::Unauthorized.new if authorize_request.is_regular?
  end
end
