# app/controllers/rest/customers_controller.rb
class Rest::CustomersController < ApplicationController
  include Pagination
  include Sorting

  before_action :authorize_request, except: %i[create]
  before_action :customer, except: %i[index create]

  def show
    raise Error::Unauthorized.new if customer != authorize_request && authorize_request.is_regular?

    render json: {
      serializer: CustomerSerializers::CustomerSerializer.new(
        customer
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      records: customers_collection.count,
      offers: CustomerSerializers::CustomerSerializer.new(
        customers_collection
      ).serializable_hash
    }
  end

  def create
    form = CustomerForms::CustomerForm.new(customer_params)
    result = CustomerService::CreateCustomerService.call(form: form)

    if result.error?
      render_error(result.object)
    else
      render json: CustomerSerializers::CustomerSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def customer
    @customer ||= Customer.find(params[:id])
  end

  def customers_collection
    @customers_collection ||= CustomerService::GetCustomerService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      authorized_user: authorize_request
    )
  end

  def customer_params
    params.permit(
      :id,
      :company,
      :username,
      :email,
      :first_name,
      :last_name,
      :address,
      :city,
      :country,
      :postal_code,
      :customer_description,
      :password,
      :password_confirmation
    ).to_h.symbolize_keys
  end

  def params_for_sort
    ::Customers::ParamsForSort.allowed_values
  end
end
