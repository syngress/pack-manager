$(document).ready(function () {
});

///////////////////
// Authorization //
///////////////////
window.customerAuthorizationScope = {
  loginFunction: {
    send : function(email, password) {
      $.ajax({
        url: '/auth/login_customer',
        type: 'POST',
        dataType: 'json',
        headers: {
          'Content-Type': 'application/json',
        },
        data: JSON.stringify({ email: email, password: password }),
        success: function (data) {
          $.ajax({
            type: "GET",
            url: '/application/client_authorization',
            data: ({ token: data.token, username: data.username }),
            success: function (data) {
            },
            error: function (jqXHR, status, thrownError) {
              var responseText = jQuery.parseJSON(jqXHR.responseText);
                $("#authResult").text(responseText.details);
            }
          });
          window.location.href = `/customers/${data.id}/edit`;
        },
        error: function (jqXHR, status, thrownError) {
          var responseText = jQuery.parseJSON(jqXHR.responseText);
            $("#authResult").text(responseText.details);
        }
      });
    }
  },
  logoutFunction: {
    send : function() {
      $.ajax({
        type: "GET",
        url: '/home/logout',
        dataType: 'json',
        headers: {
          'Content-Type': 'application/json',
        },
        data: ({authorization: 'logout'}),
        error: function (jqXHR, status, thrownError) {
          var responseText = jQuery.parseJSON(jqXHR.responseText);
            $("#authResult").text(responseText.details);
        }
      });
    }
  }
};

///////////
// Offer //
///////////
window.offerScope = {
  acceptOfferFunction: {
    send : function(id) {
      $.ajax({
        url: `/offers/${id}/accept`,
        type: 'POST',
        error: function (jqXHR, status, thrownError) {
          var responseText = jQuery.parseJSON(jqXHR.responseText);
            $("#authResult").text(responseText.details);
        }
      });
    }
  },
  declineOfferFunction: {
    send : function(id) {
      $.ajax({
        url: `/offers/${id}/reject`,
        type: 'POST',
        error: function (jqXHR, status, thrownError) {
          var responseText = jQuery.parseJSON(jqXHR.responseText);
            $("#authResult").text(responseText.details);
        }
      });
    }
  }
};
