window.closeFlash = function() {
    let lastAlert = $(`#flash:last`)

    function fadeFlash(key) {
      lastAlert.animate( {opacity: 0}, 2500, function() {
        $(this).hide('slow', function() {
          $(this).remove()
        });
      });
    };

    setTimeout(fadeFlash, 2500)
};
