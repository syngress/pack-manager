require("packs/bootstrap/demo")
require("packs/bootstrap/jquery.3.2.1.min")
require("packs/bootstrap/bootstrap-datepicker")
require("packs/bootstrap/bootstrap-notify")
require("packs/bootstrap/bootstrap-switch")
require("packs/bootstrap/bootstrap.min")
require("packs/bootstrap/chartist.min")
require("packs/bootstrap/light-bootstrap-dashboard")
require("packs/bootstrap/nouislider.min")
require("packs/bootstrap/popper.min")
require("packs/main/main")
require("packs/main/rails_flash")

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()
