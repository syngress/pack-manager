# frozen_string_literal: true

module CustomerContracts
  # app/contracts/customer_contracts/customer_contract.rb
  class CustomerContract < Dry::Validation::Contract
    schema do
      optional(:company)
      required(:username).filled
      required(:email).filled
      required(:first_name).filled
      required(:last_name).filled
      required(:address).filled
      required(:city).filled
      required(:country).filled
      required(:postal_code).filled
      optional(:customer_description)
      required(:password).filled
      required(:password_confirmation).filled
    end

    rule(
      :username,
      :email,
      :first_name,
      :last_name,
      :address,
      :city,
      :country,
      :postal_code,
      :password,
      :password_confirmation
    ) do
      key.failure(I18n.t('errors.attributes.password.not_match')) if values['password'] != values['password_confirmation']
      key.failure(I18n.t('errors.attributes.email.invalid')) if email_validation(values['email']).nil?
      key.failure(I18n.t('errors.attributes.postal_code.invalid')) if postal_code_validation(values['postal_code']).nil?
      key.failure(I18n.t('errors.attributes.first_name.invalid')) if name_validation(values['first_name']).nil?
      key.failure(I18n.t('errors.attributes.last_name.invalid')) if name_validation(values['last_name']).nil?
      key.failure(I18n.t('errors.attributes.country.invalid')) if country_validation(values['country']).nil?
      key.failure(I18n.t('errors.attributes.city.invalid')) if city_validation(values['city']).nil?
    end

    def email_validation(email)
      email =~ PatternsHelper.email_pattern
    end

    def postal_code_validation(postal_code)
      postal_code =~ PatternsHelper.postal_code_pattern
    end

    def name_validation(name)
      name =~ PatternsHelper.name_pattern
    end

    def country_validation(country)
      country =~ PatternsHelper.country_pattern
    end

    def city_validation(city)
      city =~ PatternsHelper.city_pattern
    end
  end
end
