# frozen_string_literal: true

module OfferContracts
  # app/contracts/offer_contracts/offer_contract.rb
  class OfferContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:name).filled(:string)
      required(:description).filled(:string)
    end

    rule(:id, :name, :description) do
      key.failure(I18n.t('errors.attributes.name.missing')) if values[:name].nil?
      key.failure(I18n.t('errors.attributes.description.missing')) if values[:description].nil?
    end
  end
end
