# frozen_string_literal: true

module ProductContracts
  # app/contracts/product_contracts/product_contract.rb
  class ProductContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:name).filled(:string)
      required(:description).filled(:string)
      required(:quantity).filled(:integer)
      optional(:price)
    end

    rule(:id, :name, :description, :quantity) do
      attributes = product_type_attributes(values)

      key.failure(I18n.t('errors.attributes.name.too_long')) if values[:name].length > 40
      key.failure(I18n.t('errors.attributes.quantity.insufficient')) if values['quantity'] < attributes.minimum_quantity
      key.failure(I18n.t('errors.attributes.quantity.exceeded')) if values['quantity'] > attributes.maximum_quantity
    end

    def product_type_attributes(values)
      ProductAttributesHelper.attributes(values[:name])
    end
  end
end
