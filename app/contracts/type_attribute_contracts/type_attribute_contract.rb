# frozen_string_literal: true

module TypeAttributeContracts
  # app/contracts/type_attribute_contracts/type_attribute_contract.rb
  class TypeAttributeContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:product_id)
      required(:width).filled
      required(:height).filled
      optional(:length)
      optional(:material)
    end

    rule(:id, :product_id, :width, :height, :length, :material) do
      attributes = product_type_attributes(values)

      key.failure(I18n.t('errors.attributes.width.too_small')) if attributes.width.present? && values['width'] < attributes.width.first
      key.failure(I18n.t('errors.attributes.width.exceeded')) if attributes.width.present? && values['width'] > attributes.width.last
      key.failure(I18n.t('errors.attributes.height.too_small')) if attributes.height.present? && values['height'] < attributes.height.first
      key.failure(I18n.t('errors.attributes.height.exceeded')) if attributes.height.present? && values['height'] > attributes.height.last
      key.failure(I18n.t('errors.attributes.length.too_small')) if attributes.length.present? && values['length'] < attributes.length.first
      key.failure(I18n.t('errors.attributes.length.exceeded')) if attributes.length.present? && values['length'] > attributes.length.last
    end

    def product_type_attributes(values)
      product_name = Product.find(values[:product_id]).name
      ProductAttributesHelper.attributes(product_name)
    end
  end
end
