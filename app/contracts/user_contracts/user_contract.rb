# frozen_string_literal: true

module UserContracts
  # app/contracts/user_contracts/user_contract.rb
  class UserContract < Dry::Validation::Contract
    schema do
      optional(:id)
      required(:username).filled
      required(:email).filled
      required(:password).filled
      required(:password_confirmation).filled
      required(:id_role).filled
    end

    rule(:id, :username, :email, :password, :id_role) do
      key.failure(I18n.t('errors.attributes.password.not_match')) if values['password'] != values['password_confirmation']
    end
  end
end
