# frozen_string_literal: true

# app/forms/user_forms
module UserForms
  # app/forms/user_forms/user_form.rb
  class UserForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :username, Types::String
    attribute :email, Types::String
    attribute :password, Types::String
    attribute :password_confirmation, Types::String
    attribute :id_role, Types::Integer

    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= UserContracts::UserContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:name], error: result_data.values.flatten.first
    end
  end
end
