# frozen_string_literal: true

# app/forms/product_forms
module ProductForms
  # app/forms/product_forms/create_product_form.rb
  class CreateProductForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :name, Types::String
    attribute :description, Types::String
    attribute :quantity, Types::Integer
    attribute :price, Types::Strict::Decimal.meta(omittable: true)

    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= ProductContracts::ProductContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:name], error: result_data.values.flatten.first
    end
  end
end
