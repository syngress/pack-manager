# frozen_string_literal: true

# app/forms/customer_forms
module CustomerForms
  # app/forms/customer_forms/customer_form.rb
  class CustomerForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :company, Types::String
    attribute :username, Types::String
    attribute :email, Types::String
    attribute :first_name, Types::String
    attribute :last_name, Types::String
    attribute :address, Types::String
    attribute :city, Types::String
    attribute :country, Types::String
    attribute :postal_code, Types::String
    attribute :customer_description, Types::String
    attribute :password, Types::String
    attribute :password_confirmation, Types::String

    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= CustomerContracts::CustomerContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:name], error: result_data.values.flatten.first
    end
  end
end
