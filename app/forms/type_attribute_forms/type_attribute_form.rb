# frozen_string_literal: true

# app/forms/type_attribute_forms
module TypeAttributeForms
  # app/forms/type_attribute_forms/type_attribute_form.rb
  class TypeAttributeForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :width, Types::Integer
    attribute :height, Types::Integer
    attribute :length, Types::Integer.meta(omittable: true)
    attribute :material, Types::Array.of(Types::Coercible::String).meta(omittable: true)
    attribute :product_id, Types::Integer

    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= TypeAttributeContracts::TypeAttributeContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:name], error: result_data.values.flatten.first
    end
  end
end
