class Error
  # /app/models/error/unauthorized.rb
  class Unauthorized < Error
    def initialize(message = nil)
      message = I18n.t :unauthorized_call, scope: 'errors.messages'
      super(message, :token_invalid, {}, Http::Status.new(401))
    end
  end
end
