class Customer < ApplicationRecord
  has_secure_password
  has_many :offers

  validates :email, uniqueness: true, presence: true, allow_blank: false, format: { with: PatternsHelper.email_pattern }
  validates :username, uniqueness: true, presence: true, allow_blank: false, length: { minimum: 3, maximum: 30 }
  validates :first_name, presence: true, length: { minimum: 3, maximum: 50 }, format: { with: PatternsHelper.name_pattern }
  validates :last_name, presence: true, length: { minimum: 3, maximum: 50 }, format: { with: PatternsHelper.name_pattern }
  validates :postal_code, presence: true, format: { with: PatternsHelper.postal_code_pattern }
  validates :country, presence: true, format: { with: PatternsHelper.country_pattern }
  validates :city, presence: true, format: { with: PatternsHelper.city_pattern }
  validates :password_digest, presence: true

  ROLES = {
    REGULAR: 1,
    ADMIN: 2,
    SALES: 3
  }

  def is_regular?
    self.id_role == ROLES[:REGULAR]
  end

  def is_admin?
    self.id_role == ROLES[:ADMIN]
  end

  def is_salesman?
    self.id_role == ROLES[:SALES]
  end
end
