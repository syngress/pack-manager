class Offer < ApplicationRecord
  belongs_to :customer

  has_and_belongs_to_many :products

  validates_presence_of :name, :description, :status
end
