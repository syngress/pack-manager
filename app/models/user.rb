class User < ApplicationRecord
  has_secure_password
  has_many :offers, dependent: :destroy

  validates :email, uniqueness: true
  validates :username, uniqueness: true

  validates_presence_of :username, :email, :password_digest

  before_create :check_role

  ROLES = {
    REGULAR: 1,
    ADMIN: 2,
    SALES: 3
  }

  def is_regular?
    self.id_role == ROLES[:REGULAR]
  end

  def is_admin?
    self.id_role == ROLES[:ADMIN]
  end

  def is_salesman?
    self.id_role == ROLES[:SALES]
  end

  def check_role
    if id_role > ROLES.values.max || id_role < 1
      raise Error::ValidationFailed.new(I18n.t(:invalid_role, scope: 'errors.messages'))
    end
  end
end
