# frozen_string_literal: true

module Http
  # /app/models/http/status.rb
  class Status
    OK                  = 200
    CREATED             = 201
    NO_CONTENT          = 204
    BAD_REQUEST         = 400
    UNAUTHORIZED        = 401
    FORBIDDEN           = 403
    NOT_FOUND           = 404
    SERVER_ERROR        = 500
    SERVICE_UNAVAILABLE = 503

    attr_reader :status

    delegate :to_s, to: :status

    def initialize(status)
      status = status.to_i
      case status
      when Http::Status::OK,
           Http::Status::CREATED,
           Http::Status::NO_CONTENT,
           Http::Status::BAD_REQUEST,
           Http::Status::UNAUTHORIZED,
           Http::Status::FORBIDDEN,
           Http::Status::NOT_FOUND,
           Http::Status::SERVER_ERROR,
           Http::Status::SERVICE_UNAVAILABLE
        @status = status
      else
        raise ArgumentError "Unsupported HTTP Status Code #{status}"
      end
    end

    def to_i
      status
    end
  end
end
