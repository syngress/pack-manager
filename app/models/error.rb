# frozen_string_literal: true

# app/models/error.rb
class Error < StandardError
  include ActiveModel::Serialization

  attr_reader :key, :details, :http_status

  def initialize(message, key, details = {}, http_status = nil)
    raise_argument_error(I18n.t(:empty_error_message, scope: 'errors.messages')) if message.blank?
    raise_argument_error(I18n.t(:error_key_nil, scope: 'errors.messages')) if key.blank?
    raise_argument_error(I18n.t(:http_status_instance, scope: 'errors.messages', http_status: Http::Status)) unless http_status.nil? || http_status.is_a?(Http::Status)

    super(message)
    @key = key.to_sym
    @details = details
    @http_status = http_status
  end

  def as_json(_args = nil)
    {
      status: http_status.status,
      error: key,
      message: message,
      details: details,
      request_id: Thread.current[:request_id] || 'Backend-Request-Id Unavailable'
    }
  end

  def raise_argument_error(message)
    raise ArgumentError message
  end
end
