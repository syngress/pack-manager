class Product < ApplicationRecord
  belongs_to :offer

  has_many :type_attributes

  validates_presence_of :name, :description, :quantity
end
