# frozen_string_literal: true

# /app/serializers/error_event_serializer.rb
class ErrorEventSerializer
  include FastJsonapi::ObjectSerializer
  set_key_transform :underscore

  attributes :id, :cok, :error_id, :error_description, :error_date
end
