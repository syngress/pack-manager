# frozen_string_literal: true

module CustomerSerializers
  # app/serializers/customer_serializers/offer_serializer.rb
  class OfferSerializer
    include JSONAPI::Serializer
  end
end
