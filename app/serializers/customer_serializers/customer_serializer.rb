# frozen_string_literal: true

module CustomerSerializers
  # app/serializers/customer_serializers/customer_serializer.rb
  class CustomerSerializer
    include JSONAPI::Serializer

    attributes :id, :company, :username, :email, :first_name, :last_name, :address, :city, :country, :postal_code, :customer_description, :password_digest, :created_at, :updated_at

    has_many :offers
  end
end
