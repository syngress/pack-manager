# frozen_string_literal: true

module ProductSerializers
  # app/serializers/product_serializers/product_serializer.rb
  class ProductSerializer
    include JSONAPI::Serializer

    attributes :id, :name, :description, :quantity, :price, :created_at, :updated_at

    has_many :type_attributes
  end
end
