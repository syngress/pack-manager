# frozen_string_literal: true

module ProductSerializers
  # app/serializers/product_serializers/type_attribute_serializer.rb
  class TypeAttributeSerializer
    include JSONAPI::Serializer
  end
end
