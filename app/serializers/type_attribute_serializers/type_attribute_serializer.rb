# frozen_string_literal: true

module TypeAttributeSerializers
  # app/serializers/type_attribute_serializers/type_attribute_serializer.rb
  class TypeAttributeSerializer
    include JSONAPI::Serializer

    attributes :id, :width, :height, :length, :material, :product_id, :created_at, :updated_at
  end
end
