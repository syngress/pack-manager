# frozen_string_literal: true

module UserSerializers
  # app/serializers/user_serializers/user_serializer.rb
  class UserSerializer
    include JSONAPI::Serializer

    attributes :id, :username, :email, :id_role, :password_digest, :created_at, :updated_at

    attribute :id_role do |user_object|
      RoleNames.translate_role(user_object)
    end
  end
end
