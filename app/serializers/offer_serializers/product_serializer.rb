# frozen_string_literal: true

module OfferSerializers
  # app/serializers/offer_serializers/product_serializer.rb
  class ProductSerializer
    include JSONAPI::Serializer
  end
end
