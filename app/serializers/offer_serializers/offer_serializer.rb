# frozen_string_literal: true

module OfferSerializers
  # app/serializers/offer_serializers/offer_serializer.rb
  class OfferSerializer
    include JSONAPI::Serializer

    attributes :id, :name, :description, :status, :price, :customer_id, :created_at, :updated_at

    has_many :products
  end
end
