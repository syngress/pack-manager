# frozen_string_literal: true

# app/grids/products_grid.rb
class ProductsGrid
  include Datagrid

  scope do
    Offer
  end

  # Columns
  column(:id, mandatory: true)
  column(:name, mandatory: true, order: false)
  column(:description, mandatory: true, order: false) do |model|
    format(model.description) do |value|
      truncate(value, length: 25) << " ..."
    end
  end
  column(:quantity, mandatory: true, order: 'model.quantity')
  column(:price, mandatory: true)
  column('offer id', mandatory: true, order: 'model.offer_id') do |model|
    format(model) do |value|
      link_to value.offer_id, offer_path(model)
    end
  end
  column(:created_at, mandatory: true) do |record|
    record.created_at.strftime("%Y-%m-%d %H:%M")
  end
end
