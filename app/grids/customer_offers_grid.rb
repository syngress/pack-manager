# frozen_string_literal: true

# app/grids/customer_offers_grid.rb
class CustomerOffersGrid
  include Datagrid

  scope do
    Customer
  end

  # Columns
  column(:id, mandatory: true)
  column(:name, mandatory: true) do |model|
    format(model.name) do |value|
      link_to value, offer_path(model)
    end
  end
  column(:description, mandatory: true) do |model|
    format(model.description) do |value|
      value[0..35].rpartition(" ").first << " ..."
    end
  end
  column(:status, :mandatory => true) do |model|
    format(model.status) do |value|
      link_to value, "javascript:;", id: "offer_status", offer_status: value, offer_id: model.id, customer_id: model.customer.id, class: "offer_status" 
    end
  end
  column(:price, mandatory: true)
  column(:created_at, mandatory: true) do |record|
    record.created_at.strftime("%Y-%m-%d %H:%M")
  end
end
