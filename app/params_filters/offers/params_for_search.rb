# frozen_string_literal: true

module Offers
  # app/params_filters/offers/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id name user_id]
      end
    end
  end
end
