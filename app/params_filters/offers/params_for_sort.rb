# frozen_string_literal: true

module Offers
  # app/params_filters/offers/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name created_at]
      end
    end
  end
end
