# frozen_string_literal: true

module Products
  # app/params_filters/products/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id name max_quantity min_quantity price offer_id]
      end
    end
  end
end
