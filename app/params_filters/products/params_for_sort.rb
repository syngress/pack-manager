# frozen_string_literal: true

module Products
  # app/params_filters/products/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name quantity price]
      end
    end
  end
end
