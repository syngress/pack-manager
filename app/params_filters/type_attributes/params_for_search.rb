# frozen_string_literal: true

module TypeAttributes
  # app/params_filters/type_attributes/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[id name width height length material product_id]
      end
    end
  end
end
