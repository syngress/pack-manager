# frozen_string_literal: true

module TypeAttributes
  # app/params_filters/type_attributes/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id name width height length material]
      end
    end
  end
end
