# frozen_string_literal: true

module Users
  # app/params_filters/users/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id username email id_role]
      end
    end
  end
end
