# frozen_string_literal: true

module Customers
  # app/params_filters/customers/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id username email city country postal_code]
      end
    end
  end
end
