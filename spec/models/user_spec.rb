require 'rails_helper'

RSpec.describe User, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('username') }
    it { is_expected.to include('email') }
    it { is_expected.to include('password_digest') }
  end

  describe '.new' do
    subject { build(:regular_user) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.password).to eq('Qwerty11')
      expect(subject.password_confirmation).to eq('Qwerty11')
    end
  end

  describe '#username' do
    subject { build(:regular_user).username }
    it { is_expected.to be_a(String) }
  end

  describe '#email' do
    subject { build(:regular_user).email }
    it { is_expected.to be_a(String) }
  end

  describe '#password_digest' do
    subject { build(:regular_user).password_digest }
    it { is_expected.to be_a(String) }
  end

  it { should validate_presence_of(:username) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
end
