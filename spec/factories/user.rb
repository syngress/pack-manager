FactoryBot.define do
  factory :user, class: User do
    username { Faker::Name.name }
    email { Faker::Internet.free_email }
    password { "Qwerty22" }
    password_confirmation { "Qwerty22" }
    id_role { 3 }
  end

  factory :regular_user, class: User do
    username { Faker::Name.name }
    email { Faker::Internet.free_email }
    password { "Qwerty11" }
    password_confirmation { "Qwerty11" }
    id_role { 1 }
  end
end
