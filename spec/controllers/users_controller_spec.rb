require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  context 'when user does not exists' do
    # Create user object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: FactoryBot.attributes_for(:user)

        expect(response).to have_http_status(200)
        expect(User.all.count).to eql(1)
      end
    end
  end

  context 'when authorized user exists' do
    before { request.headers['Authorization'] = JsonWebToken.encode(user_id: user.id) }
    let(:user) { create(:user) }
    let(:regular_user) { create(:regular_user) }

    # Get user object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: user.id }

        expect(response).to have_http_status(200)
        expect(User.all.count).to eql(1)
      end
    end
  end

  context 'when unauthorized user exists' do
    before { request.headers['Authorization'] = nil }
    let(:regular_user) { create(:regular_user) }

    # Get user object
    describe 'GET #show' do
      it 'responds with an HTTP 401 status code for unauthorized user' do
        get :show, params: { id: regular_user.id }

        expect(response).to have_http_status(401)
      end
    end
  end
end
