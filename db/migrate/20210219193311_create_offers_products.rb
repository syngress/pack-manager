class CreateOffersProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :offers_products, id: false do |t|
      t.belongs_to :offer, index: true
      t.belongs_to :product, index: true
    end
  end
end
