class CreateTypeAttributes < ActiveRecord::Migration[6.1]
  def change
    create_table :type_attributes do |t|
      t.integer :width
      t.integer :height
      t.integer :length
      t.string :material, array: true, default: []

      t.references :product, foreign_key: true, index: { unique: true }

      t.timestamps
    end
  end
end
