class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    create_table :customers do |t|
      t.string :company
      t.string :username
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :country
      t.string :postal_code
      t.string :customer_description
      t.string :password_digest
      t.integer :id_role, default: 1

      t.timestamps
    end

    add_index :customers, :username, unique: true
    add_index :customers, :email, unique: true
  end
end
