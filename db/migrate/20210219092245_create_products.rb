class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.integer :quantity
      t.decimal :price, :precision => 8, :scale => 2

      t.references :offer, foreign_key: true

      t.timestamps
    end
  end
end
