class CreateOffers < ActiveRecord::Migration[6.1]
  def change
    create_table :offers do |t|
      t.string :name
      t.string :description
      t.string :status
      t.decimal :price, :precision => 8, :scale => 2

      t.references :customer, foreign_key: true

      t.timestamps
    end

    add_index :offers, :name, unique: true
  end
end
