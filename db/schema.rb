# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_19_193311) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.string "company"
    t.string "username"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.string "city"
    t.string "country"
    t.string "postal_code"
    t.string "customer_description"
    t.string "password_digest"
    t.integer "id_role", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["username"], name: "index_customers_on_username", unique: true
  end

  create_table "offers", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "status"
    t.decimal "price", precision: 8, scale: 2
    t.bigint "customer_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_offers_on_customer_id"
    t.index ["name"], name: "index_offers_on_name", unique: true
  end

  create_table "offers_products", id: false, force: :cascade do |t|
    t.bigint "offer_id"
    t.bigint "product_id"
    t.index ["offer_id"], name: "index_offers_products_on_offer_id"
    t.index ["product_id"], name: "index_offers_products_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "quantity"
    t.decimal "price", precision: 8, scale: 2
    t.bigint "offer_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["offer_id"], name: "index_products_on_offer_id"
  end

  create_table "type_attributes", force: :cascade do |t|
    t.integer "width"
    t.integer "height"
    t.integer "length"
    t.string "material", default: [], array: true
    t.bigint "product_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_type_attributes_on_product_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.integer "id_role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "offers", "customers"
  add_foreign_key "products", "offers"
  add_foreign_key "type_attributes", "products"
end
